# The Project

<details><summary>Click to expand</summary>

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/team-202/the-project.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/team-202/the-project/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)
</details>

## Name
EEE3088F Team 20 "The Project" Sound/Proximity HAT

## Description
This project will be designing and creating a HAT or "hardware attached on top" for the UCT STM32 Dev Kit. The HAT is to have a frequency and proximity sensor. The current use case is for a locking system.

## Visuals
https://news.mit.edu/sites/default/files/images/202206/MIT_Quantum-Sensing-01-PRESS.jpg 
Maybe...

## Installation
Software to install: MS Teams, Git, Gitbash, Atollic, C, Kicad... (more to be added)

## Usage
The door unlocks if there is an object in close proximity and a certain sound is pplayed for some time. This is to be paired with a device that emits a sound, which is not in the scope of this project.

## Support
https://teams.microsoft.com/l/team/19%3aP4NE9ADC8NRNmoZ5VnWWua3hW-QxzX29JxDW-ZCJ8Oo1%40thread.tacv2/conversations?groupId=e8aad598-cd24-4909-ae14-fc9121c0fe86&tenantId=92454335-564e-4ccf-b0b0-24445b8c03f7

## Roadmap

01. Wk01 Project start
02. Wk02 Concept Proposal 
03. Wk03 Design Proposal
04. Wk04 Initial Schematic and Simulation Design 
05. Wk05 PCB Initial Designs 
06. Wk06 Peer review of PCB Design Reports 
07. Wk07 PCB Gerber Files Hand-in
08. Wk08 Sensor Budget Tutorial
09. Wk09 Draft final project design report 
10. Wk10 PCBs returned to students
11. Wk11 Work on Report and Demo
12. Wk12 Work on Report and Demo
13. Wk13 Documentation and Software 
14. Wk14 Lab Demo / GA opportunity 2 (oral if required)

## Contributing
Need tutor help...

## Authors and acknowledgment

- Nuvit Ilkin Demirtas
- Joseph Stewart
- Si Teng Wu

## License
N/A

## Project status
Design Proposal --> Inital Design
